
// Aqui se añadirán todas las funciones que se podrán utilizar en cualquier proyecto de node que se instale esta dependencia.
module.exports = {
  /**
   * Suma de dos numeros
   * @example n1 = 1, n2 = 3 => resultado 4
   * @param {number} n1 Numero uno de la suma
   * @param {number} n2 Numero dos de la suma
   * @returns {number} Resultado de la suma
   */
  suma: function(n1, n2) {
    return n1 + n2;
  },
  /**
   * Resta de dos numeros
   * @example n1 = 5, n2 = 2 => resultado 3
   * @param {number} n1 Numero uno de la resta
   * @param {number} n2 Numero dos de la resta
   * @returns {number} Resultado de la resta
   */
  resta: function (n1, n2) {
    return n1 - n2;
  }
}